from .auth_view import SmiapLoginView, SmiapLogoutView, SmiapRegistrationView, SmiapActivationView
from .history_view import HistoryView
from .index_view import IndexView
from .link_view import LinkView
from .publications_view import PublicationView
from .staff_view import StaffView
