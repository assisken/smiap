import os

BRAND = os.getenv('BRAND')
LMS_URL = os.getenv('LMS_URL')
LMS_PASSWORD = os.getenv('LMS_PASSWORD')
DEPARTMENT = os.getenv('DEPARTMENT')
